#include "videomapstate.h"
#include "tiledvideosslavewindow.h"
#include "changesettings.h"
#include <QStringList>
#include <QApplication>
#include <QFileDialog>
#include <QFile>
#include "player.h"
#include <QDebug>
#include <cstdlib>
#include <cmath>

VideomapState* VideomapState::self(0);
QBasicAtomicInt VideomapState::flag
                            = Q_BASIC_ATOMIC_INITIALIZER(CallOnce::CO_Request);
bool exiting=false;

VideomapState *VideomapState::instance(){
   qCallOnce(init, flag);
   return self;
}

void VideomapState::del(){
    if(exiting) delete self;
}

void deleteInstance(){
    exiting=true;
    VideomapState::instance()->del();
}

void VideomapState::setZoomLayer(int nz){
    if(nz>_maxZoomLayer) _zoomlayer=_maxZoomLayer;
    else if(nz<_minZoomLayer) _zoomlayer=_minZoomLayer;
    else _zoomlayer=nz;
}

void VideomapState::init(){
    using namespace std;
    self=new VideomapState();
    atexit(deleteInstance);
}

void VideomapState::bind(){
    for(int i=0;i<_ServerPortRange;++i){
        if(QUdpSocket::bind(QHostAddress::Any,_ServerPort+i))
            break;
    }
}

void VideomapState::pingServer(bool second){
    if(!_state.testFlag(State_Server) && ! client){
        if(second || _ping>=_pingLimit){
            timer->stop();
            _state&=~State_Ping;
            disconnect(timer,SIGNAL(timeout()),
                       this,SLOT(pingServer()));
            while(_ping<_pingLimit){
                loader->inc();
                ++_ping;
            }
            if(!second){
                startServer();
            }
            loader->inc();
            startClient();
        }else{
            if(!_state.testFlag(State_Ping)){
                _state|=State_Ping;
                connect(timer,SIGNAL(timeout()),
                        this,SLOT(pingServer()));
                timer->start(_pingInterval);
            }
            ++_ping;
            send("ping");
            loader->inc();
        }
    }
}

bool VideomapState::keyPressed(int key){
    switch(key){
    case Qt::Key_O:
        openIni();
        break;
    case Qt::Key_F:
        client->toggleFullScreen();
        break;
    case Qt::Key_Q:
    case Qt::Key_Escape:
        QApplication::exit();
        break;
    case Qt::Key_C:
        config->open();
        break;
    case Qt::Key_P:
    case Qt::Key_Space:
        serverPlayer->togglePause();
        break;
    default:
        return true;
    }
    return false;
}

void VideomapState::openIni(QString s,int count){
    while((s.isEmpty() || !readTileIni(s))&&count) {
        --count;
        s=QFileDialog::getOpenFileName(0, tr("Open tile INI file..."),_tilePathTpl,"INI file (*.ini);;Any file (*)");
        s.remove(0,_tilePathTpl.length());
    }
}

void VideomapState::startClient(){
    loader->log("Starting client...");
    _state|=State_Client;
    client=new TiledVideosSlaveWindow(this->localPort());
    connect(this,SIGNAL(stateRecieved(VideomapState::ChangesMask)),
                client,SLOT(resolveTiles(VideomapState::ChangesMask)));
    //w.setWindowState(Qt::WindowFullScreen);
    loader->inc();
    delete loader;
    loader=NULL;
}

void VideomapState::startServer(){
    loader->log("Starting server...");
    connect(timer,SIGNAL(timeout()),
            this,SLOT(castState()));
    timer->start(_stateEchoInterval);
    serverPlayer=new ServerPlayer(this);
    serverPlayer->load(etalonTile());
    _state|=State_Server;
    castState();
}

QString VideomapState::etalonTile(){
    return tilePath(_etalonPos);
}

QString VideomapState::tilePath(int x, int y){
    QString tp=tileNameTpl();
    tp.replace("{x}",QVariant(x).toString(),Qt::CaseInsensitive);
    tp.replace("{y}",QVariant(y).toString(),Qt::CaseInsensitive);
    tp.replace("{z}",QVariant(_zoomlayer).toString(),Qt::CaseInsensitive);
    return tp;
}
QString VideomapState::tilePath(const QPoint& I){
    return tilePath(I.x(),I.y());
}

void VideomapState::setPosition(QPoint np){
    _position=np;
    savePosition();
}

void VideomapState::savePosition(){
    settings.setValue("position/x",_position.x());
    settings.setValue("position/y",_position.y());
}

void VideomapState::setTilePath(QString np){
    _tilePathTpl=np;
    settings.setValue("tilePathTpl",_tilePathTpl);
}

void VideomapState::setPingInterval(double np){
    _pingInterval=np*1000;
    if(_state.testFlag(State_Ping)){
        timer->stop();
        timer->start(_pingInterval);
    }
    settings.setValue("ping/interval",_pingInterval);
}

void VideomapState::setPlayerPortRange(int np){
    _PlayerPortRange=np;
    settings.setValue("player/portrange",_PlayerPortRange);
}

void VideomapState::setPingLimit(int np){
    _pingLimit=np;
    settings.setValue("ping/limit",_pingLimit);
}

void VideomapState::setStateEchoInterval(double ns){
    _stateEchoInterval=ns*1000;
    if(_state.testFlag(State_Server)){
        timer->stop();
        timer->start(_stateEchoInterval);
    }
    settings.setValue("state/interval",_stateEchoInterval);
}

void VideomapState::setServerPortRange(int ns){
    _ServerPortRange=ns;
    settings.setValue("portrange",_ServerPortRange);
}

void VideomapState::setServerPort(int ns){
    _ServerPort=ns;
    settings.setValue("port",_ServerPort);
}

void VideomapState::setPlayerPort(int np){
    _PlayerPort=np;
    settings.setValue("player/port",_PlayerPort);
}

VideomapState::State VideomapState::state(){
    return _state;
}

QString VideomapState::tileNameTpl(){
    if(QString("")==_tileNameTpl) readTileIni(tileIni);
    if(_tileNameTpl[0]=='/') return _tileNameTpl;
    if(_tileNameTpl[0]=='~') return _tileNameTpl;
    return _tilePathTpl+"/"+_tileNameTpl;
}

void VideomapState::readPendingDatagrams(){
    while (hasPendingDatagrams()) {
         QByteArray datagram;
         datagram.resize(pendingDatagramSize());
         QHostAddress sender;
         quint16 senderPort;

         readDatagram(datagram.data(), datagram.size(),
                                 &sender, &senderPort);

         QString contents(datagram.data());

         emit datagramRecieved(contents);
         unserialize(contents);
     }
}

void VideomapState::castMove(int Dx, int Dy){
    send(QString("move %1 %2").arg(Dx).arg(Dy));
}
void VideomapState::castZoom(int PosX, int PosY, double zoomDelta){
    send(QString("zoom %1 %2 %3").arg(PosX).arg(PosY).arg(zoomDelta));
}
void VideomapState::castState(){
    send(serialize());
}

void VideomapState::send(QString data){
    if(QUdpSocket::state()==BoundState){
        QByteArray dg;
        dg.append(data);
        for(int i=0;i<_ServerPortRange;++i){
            writeDatagram(dg,QHostAddress::Any,_ServerPort+i);
        }
    }
}

void VideomapState::readSettings(){
    loader->log("Read settings...");
    qDebug()<<settings.allKeys();
    settings.beginGroup("server");
    installed=settings.value("installed",0).toInt();
    _position=QPoint(settings.value("position/x",0).toInt(),
                      settings.value("position/y",0).toInt());
    _PlayerPort=settings.value("player/port",23867).toInt();
    _PlayerPortRange=settings.value("player/portrange",32).toInt();
    _tilePathTpl=settings.value("tilePathTpl",tr(
                                                #ifdef Q_WS_WIN
                                                   "C:/Users/complynx/Videos"
                                                #else
                                                   "/home/dan/Videos"
                                                #endif
                                                )).toString();
    _pingLimit=settings.value("ping/limit",3).toInt();
    loader->setMaximum(_pingLimit+5);
    _pingInterval=settings.value("ping/interval",500).toInt();
    _stateEchoInterval=settings.value("state/interval",2000).toInt();
    _ServerPort=settings.value("port",23865).toInt();
    _ServerPortRange=settings.value("portrange",2).toInt();
    tileSize=QSize(settings.value("tile/width",1920).toInt(),
                   settings.value("tile/height",1200).toInt());

    readTileIni(settings.value("lastVideo","").toString());

    loader->inc();
}

VideomapState::VideomapState() :
    QUdpSocket()
{
    client=NULL;
    serverPlayer=NULL;
    _state=State_Zero;
    loader=new Loader(15);
    loader->show();
    loader->setWindowModality(Qt::ApplicationModal);
    loader->log("Init...");

    _inc=0;

    _ping=0;
    _zoomlayer=0;
    _etalonPos=QPoint(0,0);
    readSettings();
    config=new ChangeSettings();
    loader->inc();
    connect(this, SIGNAL(readyRead()),
            this, SLOT(readPendingDatagrams()));
    bind();
    timer=new QTimer(this);
    loader->log("Ping server...");
    pingServer();
}

void VideomapState::setZoomCoefficient(double nzc){
    _zoomCoefficient=nzc;
}

bool VideomapState::readTileIni(QString name){
    if(name=="") return false;
    QSettings tile(_tilePathTpl+"/"+name,QSettings::IniFormat);
    if(tile.status()!=QSettings::NoError)
        return false;
    _tileNameTpl=tile.value("tileNameTpl").toString();
    _etalonPos.rx()=tile.value("etalon/x",0).toInt();
    _etalonPos.ry()=tile.value("etalon/y",0).toInt();
    _position.rx()=tile.value("start/x",0).toInt();
    _position.ry()=tile.value("start/y",0).toInt();
    _zoomlayer=tile.value("zoom/main",1).toInt();
    _maxZoomLayer=tile.value("zoom/max",_zoomlayer).toInt();
    _minZoomLayer=tile.value("zoom/min",_zoomlayer).toInt();
    _loop=tile.value("loop",1).toInt();
    qDebug()<<"---"<<name<<_tileNameTpl;
    setZoomCoefficient(tile.value("zoom/coefficient",4).toDouble());
    setZoomLayer(_zoomlayer);
    QFile etf(etalonTile());
    if(!etf.exists()){
        qDebug()<<"No file"<<etalonTile();
        return false;
    }
    Player::MediaInfo i=Player::getMediaInfo(etalonTile());
    tileSizeRet=tileSize=i.size;
    tileIni=name;
    settings.setValue("lastVideo",tileIni);

//    delete serverPlayer;
//    serverPlayer=new ServerPlayer(this);

    if(_state.testFlag(State_Server))
        serverPlayer->load(etalonTile());
    emit stateRecieved(ChangesMask(Changes_NewName));
    castState();

    return true;
}

void VideomapState::unserialize(QString query){
    QStringList dsep=query.split(' ');
    if(dsep[0]=="move" && dsep.length()>=3){
        _position.rx()+=dsep[1].toInt();
        _position.ry()+=dsep[2].toInt();
        emit stateRecieved(ChangesMask(Changes_Moved));
    }
    if(dsep[0]=="zoom" && dsep.length()>=4){
        ChangesMask changes(Changes_None);
        double Z=dsep[3].toDouble();
        Z=std::exp(Z);
        QPoint zpos(dsep[1].toInt(),dsep[2].toInt());
        //QPoint prel=zpos-_position;
        //zpos*=Z;
        _position-=zpos;//-prel;
        changes|=Changes_Moved;
        changes|=Changes_Resized;
        double oZ=(double)tileSize.height()/(double)tileSizeRet.height()*(double)Z;
//        qDebug()<<oZ<<Z<<dep;
        while(oZ>_zoomCoefficient){
            oZ/=_zoomCoefficient;
            setZoomLayer(zoomLayer()-1);
            changes|=Changes_NewName;
        }
        while(oZ<1/_zoomCoefficient){
            oZ*=_zoomCoefficient;
            setZoomLayer(zoomLayer()+1);
            changes|=Changes_NewName;
        }
        tileSize=tileSizeRet*oZ;
        _position*=Z;
        _position+=zpos;
        emit stateRecieved(changes);
    }else if(dsep[0]=="state"){
        ChangesMask changes(Changes_None);
        QStringList dsepval;
        for(int i=1;i<dsep.length();++i){
            dsepval=dsep[i].split('=');
            if(dsepval[0]=="x"){
                int tmp=dsepval[1].toInt();
                if(_position.rx()!=tmp){
                    changes|=Changes_Moved;
                    _position.rx()=tmp;
                }
            }else if(dsepval[0]=="y"){
                int tmp=dsepval[1].toInt();
                if(_position.ry()!=tmp){
                    changes|=Changes_Moved;
                    _position.ry()=tmp;
                }
            }else if(dsepval[0]=="width"){
                int tmp=dsepval[1].toInt();
                if(tileSize.rwidth()!=tmp){
                    changes|=Changes_Resized;
                    tileSize.rwidth()=tmp;
                }
            }else if(dsepval[0]=="height"){
                int tmp=dsepval[1].toInt();
                if(tileSize.rheight()!=tmp){
                    changes|=Changes_Resized;
                    tileSize.rheight()=tmp;
                }
            }else if(dsepval[0]=="layer"){
                int tmp=dsepval[1].toInt();
                if(_zoomlayer!=tmp){
                    changes|=Changes_NewName;
                    setZoomLayer(tmp);
                }
            }else if(dsepval[0]=="name"){
                if(tileIni!=dsepval[1]){
                    changes|=Changes_NewName;
                    readTileIni(dsepval[1]);
                }
            }else if(dsepval[0]=="port"){
                int tmp=dsepval[1].toInt();
                if(_PlayerPort!=tmp){
                    changes|=Changes_PortChanges;
                    setPlayerPort(tmp);
                }
            }else if(dsepval[0]=="portRange"){
                int tmp=dsepval[1].toInt();
                if(_PlayerPortRange!=tmp){
                    changes|=Changes_PortChanges;
                    setPlayerPortRange(tmp);
                }
            }else if(dsepval[0]=="serverPR"){
                int tmp=dsepval[1].toInt();
                if(_ServerPortRange!=tmp){
                    //changes=ChangesMask_PortChanges;
                    setServerPortRange(tmp);
                }
            }
        }
        pingServer(true);
        emit stateRecieved(changes);
    }else if(dsep[0]=="ping"){
        if(_state.testFlag(State_Server)){
            castState();
        }
    }
}

VideomapState::~VideomapState(){
    if(client) delete client;
    if(loader) delete loader;
    if(serverPlayer) delete serverPlayer;
    close();
}

QString VideomapState::serialize(){
    QString ret("state x=%1 y=%2 width=%3 height=%4 name=%5 port=%6 portRange=%7 serverP=%8 serverPR=%9");
    return ret.arg(_position.rx()).arg(_position.ry())
            .arg(tileSize.rwidth()).arg(tileSize.rheight())
            .arg(tileIni)
            .arg(_PlayerPort).arg(_PlayerPortRange).arg(_ServerPort).arg(_ServerPortRange);
}
