#ifdef TEST___
#include <iostream>
#include <string>
#endif

template<class Key,class T> class Container{
  struct Container_c{
	Key key;
	T val;
	Container_c*next;
	Container_c*prev;
  };
  Container_c*first;
  Container_c*i;
  int count;
public:
  Container(){i=first=NULL;count=0;}
  void insert(Key k,T v){
	Container_c*n=new Container_c();
	n->key=k;
	n->val=v;
	n->next=first;
	n->prev=NULL;
	if(first)first->prev=n;
	first=n;
	++count;
  }
  void insertOrUpdate(Key k,T v){
	if(find(k)) i->val=v;
	else insert(k,v);
  }
  bool find(Key k){
	for(i=first;i;i=i->next)
	  if(i->key==k) return true;
	return false;
  }
  bool findVal(T v){
	for(i=first;i;i=i->next)
	  if(i->val==v) return true;
	return false;
  }
  bool contains(Key k){
	Container_c*i;
	for(i=first;i;i=i->next)
	  if(i->key==k) return true;
	return false;
  }
  bool containsVal(T v){
	Container_c*i;
	for(i=first;i;i=i->next)
	  if(i->val==v) return true;
	return false;
  }
  T& value(){return i->val;}
  Key& key(){return i->key;}
  void rewind(){i=first;}
  bool end(){return i==NULL;}
  void inc(){i=i?i->next:i;}
  void del(){
	if(i){
	  Container_c*n=i;
	  i=n->next;
	  if(!n->prev){
		if(!n->next) first=NULL;
		else first=n->next;
	  }
	  if(n->prev) n->prev->next=n->next;
	  if(n->next) n->next->prev=n->prev;
	  --count;
	  delete n;
	}
  }
  int length(){return count;}
  bool empty(){return first==NULL;}
  void del(Key k){
	for(i=first;i;i=i->next)
	  if(i->key==k) del();
  }
};

template<class T> class Stack{
  struct cStack{
	T val;
	cStack*prev;
  };
  cStack *last;
public:
  Stack(){
	last=0;
  }
  void push(T el){
	cStack*p=new cStack();
	p->val=el;
	p->prev=last;
	last=p;
  }
  T pop(){
	T val=last->val;
	cStack *p=last;
	last=last->prev;
	delete p;
	return val;
  }
  T &value(){
	return last->val;
  }
  T &value(int i){
	cStack*j;
	for(j=last;j && i;--i,j=j->prev);
	return j->val;
  }
  bool isEmpty(){return last==0;}
  int length(){
	int i=0;
	for(cStack*j=last;j;j=j->prev) ++i;
	return i;
  }
};

#ifdef TEST___
using namespace std;

typedef Container<string,string> Map;
typedef Stack<int> ISt;

int main(int ac,char**av){
  Map* M=new Map();
  M->insert("str1","str1");
  M->insert("str2","2");
  M->insert("str3","strfasf3");
  M->insert("str4","str4as");
  M->insert("str5","str5sad");
  
  M->insertOrUpdate("str4","4updated");
  M->insertOrUpdate("str6","6");
  
  for(M->rewind();!M->end();M->inc()){
	cout<<(M->key())<<" "<<(M->value())<<"\n";
  }
  cout<<"\n";
  
  if(!M->find("st")) cout<<"NF\n";
  if(M->find("str1")) cout<<(M->key())<<" "<<(M->value())<<"\n";
  if(!M->findVal("2s")) cout<<"NF\n";
  if(M->findVal("2")) cout<<(M->key())<<" "<<(M->value())<<"\n";
	
  cout<<"\n";
  
  if(M->contains("str3")) cout<<(M->key())<<" "<<(M->value())<<"\n";
  if(M->contains("stra")) cout<<(M->key())<<" "<<(M->value())<<"\n";
	
  cout<<"\n";
  
  M->del();
  M->del("str6");
  M->del("str3");
  for(M->rewind();!M->end();M->inc()){
	cout<<(M->key())<<" "<<(M->value())<<"\n";
  }
  cout<<M->length()<<"\n";
  cout<<"\n";
  
  M->rewind();
  M->del();
  M->del();
  cout<<M->empty()<<" "<<M->length()<<"\n";
  M->del();
  for(M->rewind();!M->end();M->inc()){
	cout<<(M->key())<<" "<<(M->value())<<"\n";
  }
  cout<<M->empty()<<" "<<M->length()<<"\n";
  cout<<"\n--------------\n";
  
  ISt s;
  
  cout<<s.length()<<"\n";
  s.push(10);
  s.push(20);
  s.push(30);
  cout<<s.length()<<"\n";
  cout<<s.value()<<" "<<s.value(2)<<"\n";
  
  cout<<s.pop()<<" ";cout<<s.pop()<<" | ";
  cout<<s.length()<<"\n";
  cout<<s.isEmpty()<<" ";cout<<s.pop()<<" ";cout<<s.isEmpty()<<"\n";
  
  return 0;
}
#endif

