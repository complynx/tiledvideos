#ifndef LOADER_H
#define LOADER_H

#ifndef QT5
#include <QWidget>
#else
#include <QtWidgets/QWidget>
#endif

namespace Ui {
    class Loader;
}

class Loader : public QWidget
{
    Q_OBJECT

public:
    explicit Loader(int stages=5,QWidget *parent = 0);
    ~Loader();
    void inc();
    void setMaximum(int);

private:
    Ui::Loader *ui;
public slots:
    void log(QString);
};

#endif // LOADER_H
