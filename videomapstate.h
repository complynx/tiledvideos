#ifndef VIDEOMAPSTATE_H
#define VIDEOMAPSTATE_H

#include "call_once.h"
#include <QObject>
#include <QUdpSocket>
#include <QTimer>
#include <QPoint>
#include <QSize>
#include <QSettings>
#include "serverplayer.h"

#include "loader.h"

class TiledVideosSlaveWindow;
class ChangeSettings;

class VideomapState : public QUdpSocket
{
    Q_OBJECT
    Q_PROPERTY(State state READ state)
    Q_PROPERTY(QPoint position READ position WRITE setPosition)
    Q_PROPERTY(QString tilePathTpl READ tilePathTpl WRITE setTilePath)
    Q_PROPERTY(int playerPortRange READ playerPortRange WRITE setPlayerPortRange)
    Q_PROPERTY(int playerPort READ playerPort WRITE setPlayerPort)
    Q_PROPERTY(double pingInterval READ pingInterval WRITE setPingInterval)
    Q_PROPERTY(int pingLimit READ pingLimit WRITE setPingLimit)
    Q_PROPERTY(double stateEchoInterval READ stateEchoInterval WRITE setStateEchoInterval)
    Q_PROPERTY(int serverPortRange READ serverPortRange WRITE setServerPortRange)
    Q_PROPERTY(int serverPort READ serverPort WRITE setServerPort)
    Q_PROPERTY(int zoomLayer READ zoomLayer WRITE setZoomLayer)
    Q_PROPERTY(double zoomCoefficient READ zoomCoefficient WRITE setZoomCoefficient)
    Q_PROPERTY(int inc READ inc)
    Q_PROPERTY(int loop READ loop)
    Q_FLAGS(ChangesMask)
    Q_FLAGS(State)
public:
    enum ChangesFlag{
        Changes_None=0,
        Changes_Moved=1,
        Changes_Resized=1<<1,
        Changes_NewName=1<<2,
        Changes_PortChanges=1<<3,
        Changes_Full=Changes_NewName|Changes_PortChanges
    };
    Q_DECLARE_FLAGS(ChangesMask,ChangesFlag)
    enum States{
        State_Zero=0,
        State_Ping=1,
        State_Server=1<<1,
        State_Client=1<<2
    };
    Q_DECLARE_FLAGS(State,States)
private:
    State _state;
    QString _tileNameTpl;
    explicit VideomapState();
    ~VideomapState();
    static void init();
    Q_DISABLE_COPY(VideomapState)

    static VideomapState *self;
    static QBasicAtomicInt flag;
    QPoint _position;
    QString _tilePathTpl;
    QPoint _etalonPos;
    int installed;
    int _PlayerPortRange;
    int _pingInterval;
    int _pingLimit;
    int _stateEchoInterval;
    int _ServerPortRange;
    int _ServerPort;
    int _PlayerPort;
    int _zoomlayer;
    int _loop;
    double _zoomCoefficient;
    int _maxZoomLayer;
    int _minZoomLayer;
    int _ping;
    QSize tileSizeRet;
    int _inc;
public:

    static VideomapState* instance();
    QTimer *timer;
    Loader* loader;
    TiledVideosSlaveWindow *client;
    ServerPlayer *serverPlayer;
    ChangeSettings *config;
    QSettings settings;
    ChangesMask changes;
    inline const QPoint& position(){return _position;}
    void setPosition(QPoint);
    void savePosition();
    QString tileIni;
    inline const QString& tilePathTpl(){return _tilePathTpl;}
    void setTilePath(QString);
    inline double pingInterval(){return (double)_pingInterval/1000.;}
    void setPingInterval(double);
    inline int playerPortRange(){return _PlayerPortRange;}
    void setPlayerPortRange(int);
    inline int pingLimit(){return _pingLimit;}
    void setPingLimit(int);
    inline double stateEchoInterval(){return (double)_stateEchoInterval/1000.;}
    void setStateEchoInterval(double);
    inline int serverPortRange(){return _ServerPortRange;}
    void setServerPortRange(int);
    inline int serverPort(){return _ServerPort;}
    void setServerPort(int);
    inline int playerPort(){return _PlayerPort;}
    void setPlayerPort(int);
    inline int zoomLayer(){return _zoomlayer;}
    void setZoomLayer(int);
    inline double zoomCoefficient(){return _zoomCoefficient;}
    void setZoomCoefficient(double);
    inline int loop(){return _loop;}
    inline int inc(){return _inc++;}
    QSize tileSize;

    void readSettings();
    State state();
    void unserialize(QString query);
    void del();
    void startServer();
    bool keyPressed(int);
    void openIni(QString s=QString(),int count=1);
    void startClient();
    QString tilePath(int x,int y);
    QString tilePath(const QPoint& I);
    QString tileNameTpl();
    QString etalonTile();
    bool readTileIni(QString);
    QString serialize();
    void bind();
    inline bool operator ==(const VideomapState&b){
        return (_position==b._position
                && tileIni==b.tileIni
                && _PlayerPortRange==b._PlayerPortRange
                && _PlayerPort==b._PlayerPort
                && _ServerPortRange==b._ServerPortRange
                && _ServerPort==b._ServerPort
                && tileSize==b.tileSize);
    }
    inline bool operator !=(const VideomapState&b){
        return !operator ==(b);
    }

signals:
    void stateRecieved(VideomapState::ChangesMask);
    void datagramRecieved(QString contents);

public slots:
    void pingServer(bool second=false);
    void castMove(int Dx,int Dy);
    void castZoom(int PosX,int PosY,double zoomDelta);
    void send(QString data);
    void castState();
    void readPendingDatagrams();
};


Q_DECLARE_OPERATORS_FOR_FLAGS(VideomapState::ChangesMask)
Q_DECLARE_OPERATORS_FOR_FLAGS(VideomapState::State)




#endif // VIDEOMAPSTATE_H
