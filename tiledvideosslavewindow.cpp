#include "tiledvideosslavewindow.h"
#ifndef QT5
#include <QApplication>
#include <QDesktopWidget>
#else
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets>
#endif
#include <QMouseEvent>
#include "QTextStream"
#include <QDataStream>
#include "cstdio"
#include "cmath"
#include "QPointPower.h"


#define RPR(a) '('<<(a).left()<<','<<(a).top()<<'|'<<(a).right()<<','<<(a).bottom()<<')'
#define PPR(a) '('<<(a).x()<<','<<(a).y()<<')'

void TiledVideosSlaveWindow::initSettings(QString title){
    settings.beginGroup(title);
    _instancePosition=QPoint(settings.value("position/x",0).toInt(),
                            settings.value("position/y",0).toInt());

    resize(settings.value("window/width",100).toInt(),
           settings.value("window/height",100).toInt());
    move(settings.value("window/x",10).toInt(),
         settings.value("window/y",10).toInt());
    setWindowState((Qt::WindowState)settings.value("window/state",Qt::WindowNoState).toInt());
}

void TiledVideosSlaveWindow::resizeEvent(QResizeEvent *ev){
    if(isVisible()){
        settings.setValue("window/width",size().width());
        settings.setValue("window/height",size().height());
        resolveTiles();
    }
}

void TiledVideosSlaveWindow::moveEvent(QMoveEvent *ev){
    if(isVisible()){
        settings.setValue("window/x",pos().x());
        settings.setValue("window/y",pos().y());
    }
}

void TiledVideosSlaveWindow::wheelEvent(QWheelEvent *ev){
    QPoint p=_instancePosition+ev->globalPos();
    VideomapState::instance()->castZoom(p.x(),p.y(),ev->delta()/12000.);
}

TiledVideosSlaveWindow::TiledVideosSlaveWindow(int port)
    : QWidget()//,listener(this)//,config(this)
{
    QTextStream err(stderr,QIODevice::WriteOnly);
    show();
    initSettings(tr("instance%1").arg(port));
}

void TiledVideosSlaveWindow::toggleFullScreen(){
    if(windowState()==Qt::WindowFullScreen)
        setWindowState(Qt::WindowNoState);
    else
        setWindowState(Qt::WindowFullScreen);
    settings.setValue("windowState",(int)windowState());
}


void TiledVideosSlaveWindow::resolveTiles(VideomapState::ChangesMask changes){
    VideomapState *s=VideomapState::instance();
    QTextStream err(stderr,QIODevice::WriteOnly);
    QPoint relative=_instancePosition-s->position(),I,I0;
    I0.rx()=floor((double)relative.rx()/(double)s->tileSize.width());
    I0.ry()=floor((double)relative.ry()/(double)s->tileSize.height());
    QRect geom(QPoint(0,0),s->tileSize),instance(relative,size());
    QRect screen(QPoint(0,0),size());


    //PMap::iterator i=players.begin();
//    QMutableMapIterator<PMap::key_type,PMap::mapped_type> i(players);
//    while (i.hasNext()) {
//        i.next();
//        geom.moveTopLeft(i.key()*geom.size());
//        i.value()->move(geom.topLeft()-relative);
//        i.value()->resize(geom.size());
//        qDebug()<<i.key();
//        if(i.value()->geometry().intersects(screen) && !((int)changes&VideomapState::Changes_Full)){
//         //++i;
//        }else{
//            //i.value()->kill();
//            //delete i.value();
//            i.remove();//=players.erase(i);
//            //++i;
//        }
//    }
    players.rewind();
    while(!players.end()){
        geom.moveTopLeft(players.key()*geom.size());
        players.value()->move(geom.topLeft()-relative);
        players.value()->resize(geom.size());
        if(players.value()->geometry().intersects(screen) && !((int)changes&VideomapState::Changes_Full)){
            players.inc();
        }else{
            //i.value()->kill();
            //delete players.value();
            players.value()->stop();
            idle.push(players.value());
            players.del();
            //++i;
        }
    }
//    while(i!=players.end()){
//        geom.moveTopLeft(i.key()*geom.size());
//        i.value()->move(geom.topLeft()-relative);
//        i.value()->resize(geom.size());
//        qDebug()<<i.key();
//        if(i.value()->geometry().intersects(screen) && !((int)changes&VideomapState::Changes_Full)){
//            ++i;
//        }else{
//            //i.value()->kill();
//            //delete i.value();
//            i=players.erase(i);
//            //++i;
//        }
//    }
//    err<<"-----\n";
    for(I=I0,geom.moveTopLeft(I*geom.size());
        instance.right()>=geom.left();
        I.rx()+=1,geom.moveTopLeft(I*geom.size())
        ){
        for(I.ry()=I0.ry(),geom.moveTopLeft(I*geom.size());
            instance.bottom()>=geom.top();
            I.ry()+=1,geom.moveTopLeft(I*geom.size())
            ){
            if(/*!players.find(I)*/!players.contains(I)){
                Player *player;
                if(idle.isEmpty()) player=new Player(this);
                else player=idle.pop();
                player->move(geom.topLeft()-relative);
//                err<<PPR(I)<<PPR(geom.topLeft()-relative)<<'\n';
                player->resize(geom.size());
                player->load(s->tilePath(I));
                player->show();
                player->play();
                players.insert(I,player);
            }
        }
    }
}



void TiledVideosSlaveWindow::mousePressEvent(QMouseEvent *event){
    if(event->button()==Qt::LeftButton){
        oldmp=event->globalPos();
    }
}
void TiledVideosSlaveWindow::mouseReleaseEvent(QMouseEvent *event){
    if(event->button()==Qt::LeftButton){
        if(event->modifiers().testFlag(Qt::ShiftModifier)){
            setInstancePosition(_instancePosition+oldmp-event->globalPos());

                    /*QPoint(settings.value("position/x",0).toInt(),
                                    settings.value("position/y",0).toInt());*/
        }else{
            VideomapState::instance()->castMove(event->globalPos().x()-oldmp.x(),
                                            event->globalPos().y()-oldmp.y());
        }
    }
}

void TiledVideosSlaveWindow::setInstancePosition(QPoint ni){
    if(_instancePosition!=ni){
        _instancePosition=ni;
        settings.setValue("position/x",_instancePosition.x());
        settings.setValue("position/y",_instancePosition.y());
        resolveTiles();
    }
}

TiledVideosSlaveWindow::~TiledVideosSlaveWindow(){
    // FIXME: falling with sigsegv when destructs children.
}
