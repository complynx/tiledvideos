#ifndef QMPPROCESSNOWIDGET_H
#define QMPPROCESSNOWIDGET_H

#include <QProcess>
#include <QSize>
#include <QHash>
#include <QTimer>
#include <QTemporaryFile>
#include <QPointer>
//#define QMP_DEBUG_OUTPUT

// A custom QProcess designed for the MPlayer slave interface
class QMPProcessNowidget : public QProcess
{
    Q_OBJECT
    Q_ENUMS(state)
protected:
    bool m_exiting;

public:
    enum State {
            NotStartedState = -1,
            IdleState,
            LoadingState,
            StoppedState,
            PlayingState,
            BufferingState,
            PausedState,
            ErrorState
    };

    struct MediaInfo {
        QString videoFormat;
        int videoBitrate;
        QSize size;
        double framesPerSecond;

        QString audioFormat;
        double audioBitrate;
        int sampleRate;
        int numChannels;

        QHash<QString, QString> tags;

        bool ok;
        double length;
        bool seekable;

        MediaInfo();
    };

    QMPProcessNowidget(QObject *parent = 0);

    ~QMPProcessNowidget();

    // Starts the MPlayer process in idle mode
    void start(const QStringList &args);

    QString mplayerVersion();

    inline QProcess::ProcessState processState() const
    {
        return QProcess::state();
    }

    void writeCommand(const QString &command);

    void quit();

    inline void pause(){writeCommand("pause");}
    inline void stop(){writeCommand("stop");}

signals:
    void stateChanged(int state);
    void streamPositionChanged(double position);
    void error(const QString &reason);

    void readStandardOutput(const QString &line);
    void readStandardError(const QString &line);

private slots:
    void readStdout();
    void readStderr();
    void finished();// Called if the *process* has finished
    void movieFinished();

protected:
    void parseLine(const QString &line);// Parses a line of MPlayer output
    void parseMediaInfo(const QString &line);// Parses MPlayer's media identification output
    void parsePosition(const QString &line);// Parsas MPlayer's position output
    virtual void changeState(State state, const QString &comment = QString());// Changes the current state, possibly emitting multiple signals
    void resetValues();// Resets the media info and position value
    void writeFakeInputconf(QIODevice *device);// Writes a dummy input configuration to the given device

public:
    State m_state;
    bool multicast;

    QString m_mplayerPath;
    QString m_videoOutput;
    QString m_pipe;

    MediaInfo m_mediaInfo;
    double m_streamPosition; // This is the video position
    QTimer m_movieFinishedTimer;

    QString m_currentTag;

    QTemporaryFile *m_fakeInputconf;
};

#endif // QMPPROCESSNOWIDGET_H
