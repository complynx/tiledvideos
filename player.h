#ifndef PLAYER_H
#define PLAYER_H

#include "qmpwidget.h"

class Player : public QMPwidget
{
    Q_OBJECT
public:
    explicit Player(QWidget *parent = 0);
    void showEvent(QShowEvent *event);
    void testPlayer();
    void keyPressEvent(QKeyEvent *event);
    static MediaInfo getMediaInfo(QString file);
    void load(const QString &url);
    
public slots:
signals:
};

#endif // PLAYER_H
