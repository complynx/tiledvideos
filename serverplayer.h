#ifndef SERVERPLAYER_H
#define SERVERPLAYER_H

#include "qmpnowidget.h"

class ServerPlayer : public QMPnowidget
{
    Q_OBJECT
public:
    explicit ServerPlayer(QObject *parent = 0);
    void testPlayer();
    void load(const QString&url);

signals:

public slots:

    void printErrors(QString err);

};

#endif // SERVERPLAYER_H
