#include "player.h"
#ifndef QT5
#include <QApplication>
#include <QFileDialog>
#else
#include <QtWidgets/QApplication>
#include <QtWidgets/QFileDialog>
#endif
#include <QKeyEvent>
#include <QSettings>
#include <QProcess>
#include <QMoveEvent>
#include "videomapstate.h"


Player::Player(QWidget *parent) :
    QMPwidget(parent)
{
    QSettings settings;
    VideomapState *vi=VideomapState::instance();
    QString mpfile=settings.value(tr("mplayer"),tr("mplayer")).toString();
    setMPlayerPath(mpfile);
    testPlayer();

    QStringList args;
#ifdef Q_WS_WIN
    args.append(QString::fromUtf8("-vo"));
    args.append(QString::fromUtf8("gl2"));
#endif
    args.append(QString::fromUtf8("-nosound"));
    args.append(QString::fromUtf8("-udp-slave"));
    args.append(QString::fromUtf8("-udp-slave-port"));
    args.append(vi->property("playerPort").toString());
    args.append(QString::fromUtf8("-udp-port-range"));
    args.append(vi->property("playerPortRange").toString());
    args.append(vi->property("playerPortRange").toString());
    args.append(QString::fromUtf8("-noop"));
    args.append(vi->property("inc").toString());
    //args.append(QString::fromUtf8("-noop"));
    //args.append(vi->tileIni);
    QMPwidget::start(args);
}

void parseMediaInfo(Player::MediaInfo &m_mediaInfo,const QString &line)
{
    QStringList info = line.split("=");
    static QString m_currentTag;
    if (info.count() < 2) {
        return;
    }

    if (info[0] == "ID_VIDEO_FORMAT") {
        m_mediaInfo.videoFormat = info[1];
    } else if (info[0] == "ID_VIDEO_BITRATE") {
        m_mediaInfo.videoBitrate = info[1].toInt();
    } else if (info[0] == "ID_VIDEO_WIDTH") {
        m_mediaInfo.size.setWidth(info[1].toInt());
    } else if (info[0] == "ID_VIDEO_HEIGHT") {
        m_mediaInfo.size.setHeight(info[1].toInt());
    } else if (info[0] == "ID_VIDEO_FPS") {
        m_mediaInfo.framesPerSecond = info[1].toDouble();

    } else if (info[0] == "ID_AUDIO_FORMAT") {
        m_mediaInfo.audioFormat = info[1];
    } else if (info[0] == "ID_AUDIO_BITRATE") {
        m_mediaInfo.audioBitrate = info[1].toInt();
    } else if (info[0] == "ID_AUDIO_RATE") {
        m_mediaInfo.sampleRate = info[1].toInt();
    } else if (info[0] == "ID_AUDIO_NCH") {
        m_mediaInfo.numChannels = info[1].toInt();

    } else if (info[0] == "ID_LENGTH") {
        m_mediaInfo.length = info[1].toDouble();
    } else if (info[0] == "ID_SEEKABLE") {
        m_mediaInfo.seekable = (bool)info[1].toInt();

    } else if (info[0].startsWith("ID_CLIP_INFO_NAME")) {
        m_currentTag = info[1];
    } else if (info[0].startsWith("ID_CLIP_INFO_VALUE") && !m_currentTag.isEmpty()) {
        m_mediaInfo.tags.insert(m_currentTag, info[1]);
    }
    if(!info[0].startsWith("ID_CLIP_INFO_NAME")) m_currentTag="";
}
Player::MediaInfo Player::getMediaInfo(QString file){
    QProcess p;
    QSettings settings;
    QStringList args;
    args+="-nosound";
    args+="-vo";
    args+="null";
    args+="-ss";
    args+="03:00:00";
    args+="-really-quiet";
    args+="-identify";
    args+=file;
    MediaInfo ret;
    p.start(settings.value(tr("mplayer"),tr("mplayer")).toString(), args);
    if (!p.waitForStarted()) {
        return ret;
    }
    if (!p.waitForFinished()) {
        return ret;
    }

    QString output = QString(p.readAll());
    QStringList infos=output.split('\n');
    for(int i=0;i<infos.length();++i){
        parseMediaInfo(ret,infos[i]);
    }
    ret.ok=true;
    return ret;
}

void Player::testPlayer(){
    QString ver,mpfile;
    int a=0;
    while(QString()==(ver=mplayerVersion())){
        mpfile=QFileDialog::getOpenFileName(this, tr("MPlayer executable"));
        setMPlayerPath(mpfile);
        ++a;
        Q_ASSERT_X(a<10,"MPlayer executable selecting","Maximum iterations exceeded");
    }
    if(a){
        QSettings s;
        s.setValue(tr("mplayer"),mpfile);
    }
}


void Player::load(const QString &url){
    QMPwidget::load(url);
    QMPwidget::writeCommand(QString::fromUtf8("loop %1").arg(VideomapState::instance()->property("loop").toInt()));
}

void Player::keyPressEvent(QKeyEvent *event){
    if(VideomapState::instance()->keyPressed(event->key()))
        QMPwidget::keyPressEvent(event);
}

void Player::showEvent(QShowEvent *event){
    if (!event->spontaneous() && state() == QMPProcessNowidget::IdleState) {
//        QMPwidget::load(m_url);
    }
}
