#include "serverplayer.h"
#include <QSettings>
#include <QFileDialog>
#include "videomapstate.h"
#include <iostream>

ServerPlayer::ServerPlayer(QObject *parent) :
    QMPnowidget(parent)
{
    QSettings settings;
    VideomapState *vi=VideomapState::instance();
    QString mpfile=settings.value(tr("mplayer"),tr("mplayer")).toString();
    setMPlayerPath(mpfile);
    testPlayer();

    QStringList args;
    args.append(QString::fromUtf8("-nosound"));
    args.append(QString::fromUtf8("-vo"));
    args.append(QString::fromUtf8("null"));
    args.append(QString::fromUtf8("-udp-master"));
    args.append(QString::fromUtf8("-udp-master-port"));
    args.append(vi->property("playerPort").toString());
    args.append(QString::fromUtf8("-udp-port-range"));
    args.append(vi->property("playerPortRange").toString());
    args.append(QString::fromUtf8("-noop"));
    args.append(vi->property("inc").toString());
    connect(this,SIGNAL(readStandardError(QString)),this,SLOT(printErrors(QString)));
    QMPnowidget::start(args);
}

void ServerPlayer::load(const QString &url){
    QMPnowidget::load(url);
    QMPnowidget::writeCommand(QString::fromUtf8("loop %1").arg(VideomapState::instance()->property("loop").toInt()));
}

void ServerPlayer::printErrors(QString err){
    std::cerr<<err.toStdString()<<'\n';
}

void ServerPlayer::testPlayer(){
    QString ver,mpfile;
    int a=0;
    while(QString()==(ver=mplayerVersion()) || !multicast()){
        mpfile=QFileDialog::getOpenFileName(0, tr("MPlayer executable"));
        setMPlayerPath(mpfile);
        ++a;
        Q_ASSERT_X(a<10,"MPlayer executable selecting","Maximum iterations exceeded");
    }
    if(a){
        QSettings s;
        s.setValue(tr("mplayer"),mpfile);
    }
}
