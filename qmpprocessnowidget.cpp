#include "qmpprocessnowidget.h"
#include <QTextStream>
#include <QDebug>

QMPProcessNowidget::QMPProcessNowidget(QObject *parent) :
    QProcess(parent), m_state(NotStartedState), m_mplayerPath("C:/Program Files (x86)/SMPlayer/mplayer/mplayer.exe"),
    m_fakeInputconf(NULL)
{
    resetValues();
    m_exiting=false;


    m_videoOutput = "null";

    m_movieFinishedTimer.setSingleShot(true);
    m_movieFinishedTimer.setInterval(100);

    connect(this, SIGNAL(readyReadStandardOutput()), this, SLOT(readStdout()));
    connect(this, SIGNAL(readyReadStandardError()), this, SLOT(readStderr()));
    connect(this, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(finished()));
    connect(&m_movieFinishedTimer, SIGNAL(timeout()), this, SLOT(movieFinished()));
}

QMPProcessNowidget::~QMPProcessNowidget(){
    if (m_fakeInputconf != NULL) {
        delete m_fakeInputconf;
    }
    //kill();
}

void QMPProcessNowidget::start(const QStringList &args){

    // Figure out the mplayer version in order to check if
    // "-input nodefault-bindings" is available
    bool useFakeInputconf = false;
    QString version = mplayerVersion();
    if (version.contains("SVN")) { // Check revision
        QRegExp re("SVN-r([0-9]*)");
        if (re.indexIn(version) > -1) {
            int revision = re.cap(1).toInt();
            if (revision >= 28878) {
                useFakeInputconf = false;
            }
        }
    }

    QStringList myargs;
    myargs += "-slave";
    myargs += "-idle";
    myargs += "-noquiet";
    myargs += "-identify";
    myargs += "-nomouseinput";
    myargs += "-nokeepaspect";
    myargs += "-monitorpixelaspect";
    myargs += "1";
    if (!useFakeInputconf) {
        myargs += "-input";
        myargs += "nodefault-bindings:conf=/dev/null";
    } else {
#ifndef Q_WS_WIN
        // Ugly hack for older versions of mplayer (used in kmplayer and other)
        if (m_fakeInputconf == NULL) {
            m_fakeInputconf = new QTemporaryFile();
            if (m_fakeInputconf->open()) {
                writeFakeInputconf(m_fakeInputconf);
            } else {
                delete m_fakeInputconf;
                m_fakeInputconf = NULL;
            }
        }
        if (m_fakeInputconf != NULL) {
            myargs += "-input";
            myargs += QString("conf=%1").arg(m_fakeInputconf->fileName());
        }
#endif
    }


    myargs += args;
#ifdef QMP_DEBUG_OUTPUT
    qDebug() << myargs.join(" ");
#endif
    QProcess::start(m_mplayerPath, myargs);
    this->changeState(IdleState);
}

QString QMPProcessNowidget::mplayerVersion(){
    QProcess p;
    p.start(m_mplayerPath, QStringList("-udp-multicast"));
    if (!p.waitForStarted()) {
        return QString();
    }
    if (!p.waitForFinished()) {
        return QString();
    }

    QString output = QString(p.readAll());
    if(output.contains("multicast",Qt::CaseInsensitive))
        multicast=true;

    QRegExp re("MPlayer2* ([^ ]*)");
    if (re.indexIn(output) > -1) {
        return re.cap(1);
    }
    return output;
}

void QMPProcessNowidget::writeCommand(const QString &command){
#ifdef QMP_DEBUG_OUTPUT
    qDebug("in: \"%s\"", qPrintable(command));
#endif
    QProcess::write(command.toLocal8Bit()+"\n");
}

void QMPProcessNowidget::quit(){
    m_exiting=true;
    writeCommand("quit");
    //QProcess::waitForFinished(100);
    if (QProcess::state() == QProcess::Running) {
        QProcess::kill();
    }
    //QProcess::waitForFinished(-1);
    m_exiting=false;
}

void QMPProcessNowidget::readStdout(){
    if(!m_exiting){
        QStringList lines = QString::fromLocal8Bit(readAllStandardOutput()).split("\n", QString::SkipEmptyParts);
        for (int i = 0; i < lines.count(); i++) {
            lines[i].remove("\r");
#ifdef QMP_DEBUG_OUTPUT
            qDebug("out: \"%s\"", qPrintable(lines[i]));
#endif
            parseLine(lines[i]);
            emit readStandardOutput(lines[i]);
        }
    }
}

void QMPProcessNowidget::readStderr(){
    QStringList lines = QString::fromLocal8Bit(readAllStandardError()).split("\n", QString::SkipEmptyParts);
    for (int i = 0; i < lines.count(); i++) {
        lines[i].remove("\r");
#ifdef QMP_DEBUG_OUTPUT
        qDebug("err: \"%s\"", qPrintable(lines[i]));
#endif
        parseLine(lines[i]);
        emit readStandardError(lines[i]);
    }
}

void QMPProcessNowidget::finished(){
    this->changeState(NotStartedState);
}

void QMPProcessNowidget::movieFinished(){
    if (m_state == PlayingState) {
        this->changeState(IdleState);
    }
}

// Initialize the media info structure
QMPProcessNowidget::MediaInfo::MediaInfo()
    : videoBitrate(0), framesPerSecond(0), sampleRate(0), numChannels(0),
      ok(false), length(0), seekable(false){}

void QMPProcessNowidget::parseLine(const QString &line){
    if (line.startsWith("Playing ")) {
        this->changeState(LoadingState);
    } else if (line.startsWith("Cache fill:")) {
        this->changeState(BufferingState);
    } else if (line.startsWith("Starting playback...")) {
        m_mediaInfo.ok = true; // No more info here
        this->changeState(PlayingState);
    } else if (line.startsWith("File not found: ")) {
        this->changeState(ErrorState);
    } else if (line.endsWith("ID_PAUSED")) {
        this->changeState(PausedState);
    } else if (line.startsWith("ID_")) {
        parseMediaInfo(line);
    } else if (line.startsWith("No stream found")) {
        this->changeState(ErrorState, line);
    } else if (line.startsWith("A:") || line.startsWith("V:")) {
        if (m_state != PlayingState) {
            this->changeState(PlayingState);
        }
        parsePosition(line);
    } else if (line.startsWith("Exiting...")) {
        this->changeState(NotStartedState);
    }
}

void QMPProcessNowidget::parseMediaInfo(const QString &line){
    QStringList info = line.split("=");
    if (info.count() < 2) {
        return;
    }

    if (info[0] == "ID_VIDEO_FORMAT") {
        m_mediaInfo.videoFormat = info[1];
    } else if (info[0] == "ID_VIDEO_BITRATE") {
        m_mediaInfo.videoBitrate = info[1].toInt();
    } else if (info[0] == "ID_VIDEO_WIDTH") {
        m_mediaInfo.size.setWidth(info[1].toInt());
    } else if (info[0] == "ID_VIDEO_HEIGHT") {
        m_mediaInfo.size.setHeight(info[1].toInt());
    } else if (info[0] == "ID_VIDEO_FPS") {
        m_mediaInfo.framesPerSecond = info[1].toDouble();

    } else if (info[0] == "ID_AUDIO_FORMAT") {
        m_mediaInfo.audioFormat = info[1];
    } else if (info[0] == "ID_AUDIO_BITRATE") {
        m_mediaInfo.audioBitrate = info[1].toInt();
    } else if (info[0] == "ID_AUDIO_RATE") {
        m_mediaInfo.sampleRate = info[1].toInt();
    } else if (info[0] == "ID_AUDIO_NCH") {
        m_mediaInfo.numChannels = info[1].toInt();

    } else if (info[0] == "ID_LENGTH") {
        m_mediaInfo.length = info[1].toDouble();
    } else if (info[0] == "ID_SEEKABLE") {
        m_mediaInfo.seekable = (bool)info[1].toInt();

    } else if (info[0].startsWith("ID_CLIP_INFO_NAME")) {
        m_currentTag = info[1];
    } else if (info[0].startsWith("ID_CLIP_INFO_VALUE") && !m_currentTag.isEmpty()) {
        m_mediaInfo.tags.insert(m_currentTag, info[1]);
    }
}

void QMPProcessNowidget::parsePosition(const QString &line){
    static QRegExp rx("[ :]");
    QStringList info = line.split(rx, QString::SkipEmptyParts);

    double oldpos = m_streamPosition;
    for (int i = 0; i < info.count(); i++) {
        if ( (info[i] == "V" || info[i] == "A") && info.count() > i) {
            m_streamPosition = info[i+1].toDouble();

            // If the movie is near its end, start a timer that will check whether
            // the movie has really finished.
            if (qAbs(m_streamPosition - m_mediaInfo.length) < 1) {
                m_movieFinishedTimer.start();
            }
        }
    }

    if (oldpos != m_streamPosition) {
        emit streamPositionChanged(m_streamPosition);
    }
}

void QMPProcessNowidget::changeState(State state, const QString &comment){

    if (m_state == state) {
        return;
    }

    if (m_state == PlayingState) {
        m_movieFinishedTimer.stop();
    }

    m_state = state;
    emit stateChanged(m_state);

    switch (m_state) {
    case NotStartedState:
        resetValues();
        break;

    case ErrorState:
        emit error(comment);
        resetValues();
        break;

    default: break;
    }
}

void QMPProcessNowidget::resetValues(){
    m_mediaInfo = MediaInfo();
    multicast=false;
    m_streamPosition = -1;
}

void QMPProcessNowidget::writeFakeInputconf(QIODevice *device){
    // Query list of supported keys
    QProcess p;
    p.start(m_mplayerPath, QStringList("-input") += "keylist");
    if (!p.waitForStarted()) {
        return;
    }
    if (!p.waitForFinished()) {
        return;
    }
    QStringList keys = QString(p.readAll()).split("\n", QString::SkipEmptyParts);

    // Write dummy command for each key
    QTextStream out(device);
    for (int i = 0; i < keys.count(); i++) {
        keys[i].remove("\r");
        out << keys[i] << " " << "ignored" << endl;
    }
}
