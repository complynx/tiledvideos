#include "loader.h"
#include "ui_loader.h"

Loader::Loader(int stages,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Loader)
{
    ui->setupUi(this);
    ui->progress->setMaximum(stages);
}

void Loader::inc(){
    if(this) ui->progress->setValue(ui->progress->value()+1);
}

void Loader::log(QString m){
    if(this) ui->log->append(m);
}

void Loader::setMaximum(int m){
    if(this) ui->progress->setMaximum(m);
}

Loader::~Loader()
{
    delete ui;
}
