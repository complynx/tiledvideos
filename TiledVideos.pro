
#TEMPLATE = subdirs

#SUBDIRS += \
#    TiledVideosSlave\
#    QMPwidget \
#    TiledVideosServer \
#    StateParser


QT       += core gui

QT_VERSIONA = $$[QT_VERSION]
QT_VERSIONA = $$split(QT_VERSIONA, ".")
QT_VER_MAJ = $$member(QT_VERSIONA, 0)
QT_VER_MIN = $$member(QT_VERSIONA, 1)

!lessThan(QT_VER_MAJ, 5){
QT += widgets
DEFINES += QT5
}

TARGET = TiledVideos
TEMPLATE = app
#DESTDIR = ..

QT += network #opengl

#INCLUDEPATH += ../QMPwidget ../StateParser
#QMAKE_LIBDIR += ..
#LIBS += -lqmpwidget -lstateparser


SOURCES += main.cpp\
    player.cpp \
    tiledvideosslavewindow.cpp \
    changesettings.cpp \
    loader.cpp \
    videomapstate.cpp \
    qmpprocessnowidget.cpp \
    qmpnowidget.cpp \
    serverplayer.cpp

HEADERS  += \
    player.h \
    tiledvideosslavewindow.h \
    changesettings.h \
    portRangeGet.h \
    loader.h \
    videomapstate.h \
    call_once.h \
    qmpprocessnowidget.h \
    qmpnowidget.h \
    serverplayer.h \
    container.hpp

CONFIG += pipemode

HEADERS += \
        qmpwidget.h

SOURCES += \
        qmpwidget.cpp

!win32:pipemode: {
DEFINES += QMP_USE_YUVPIPE
HEADERS += qmpyuvreader.h
}

SOURCES +=

HEADERS += \
    QPointPower.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

FORMS += \
    changesettings.ui \
    loader.ui




























