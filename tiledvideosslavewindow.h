#ifndef TiledVideosSlaveWindow_H
#define TiledVideosSlaveWindow_H

#ifndef QT5
#include <QtGui/QWidget>
#else
#include <QtWidgets/QWidget>
#endif
#include "player.h"
#include <QSettings>
#include "portRangeGet.h"
#include "videomapstate.h"
#include "container.hpp"

inline bool operator <(const QPoint&a,const QPoint&b){
    return (a.x()<b.x()) || (a.y()<b.y());
}

typedef Container<QPoint,Player*> PCont;
typedef Stack<Player*> PStack;

class TiledVideosSlaveWindow : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QPoint instancePosition READ instancePosition WRITE setInstancePosition)
private:
    QPoint _instancePosition;
    QPoint oldmp;
    int systemport;
    QString tilePathTpl;
    QSize tileSize;

    PStack idle;
    PCont players;
public:
    QSettings settings;

    TiledVideosSlaveWindow(int port);
    void initSettings(QString settingsName=QString::fromUtf8("instance"));

    inline const QPoint& instancePosition(){return _instancePosition;}
    void setInstancePosition(QPoint);
    void toggleFullScreen();
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);


    void mousePressEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *);
    void mouseReleaseEvent(QMouseEvent *event);
    ~TiledVideosSlaveWindow();
public slots:
    void resolveTiles(VideomapState::ChangesMask changes=VideomapState::Changes_Moved);
};

#endif // TiledVideosSlaveWindow_H
