#ifndef CHANGESETTINGS_H
#define CHANGESETTINGS_H


#ifndef QT5
#include <QDialog>
#include <QAbstractButton>
#else
#include <QtWidgets/QDialog>
#include <QtWidgets/QAbstractButton>
#endif
#include <QSettings>
#include "portRangeGet.h"

namespace Ui {
    class ChangeSettings;
}

class ChangeSettings : public QDialog
{
    Q_OBJECT

public:
    explicit ChangeSettings();
    ~ChangeSettings();

private:
    Ui::ChangeSettings *ui;

public slots:
    void updateFields();
    void save(QAbstractButton*btn);
    void save();
    void open();
signals:
    void saved();
};

#endif // CHANGESETTINGS_H
