#include "qmpnowidget.h"

bool QMPnowidget::multicast(){
    if(!m_process->multicast)
        m_process->mplayerVersion();
    return m_process->multicast;
}

void QMPnowidget::togglePause(){
    if (state() == QMPProcessNowidget::PlayingState) {
        pause();
    } else if (state() == QMPProcessNowidget::PausedState) {
        play();
    }
}

/*!
 * \brief Constructor
 *
 * \param parent Parent widget
 */
QMPnowidget::QMPnowidget(QObject *parent)
    : QObject(parent)
{
    m_seekTimer.setInterval(50);
    m_seekTimer.setSingleShot(true);
    connect(&m_seekTimer, SIGNAL(timeout()), this, SLOT(delayedSeek()));

    m_process = new QMPProcessNowidget(this);
    connect(m_process, SIGNAL(stateChanged(int)), this, SIGNAL(stateChanged(int)));
//    connect(m_process, SIGNAL(streamPositionChanged(double)), this, SLOT(mpStreamPositionChanged(double)));
    connect(m_process, SIGNAL(error(const QString &)), this, SIGNAL(error(const QString &)));
    connect(m_process, SIGNAL(readStandardOutput(const QString &)), this, SIGNAL(readStandardOutput(const QString &)));
    connect(m_process, SIGNAL(readStandardError(const QString &)), this, SIGNAL(readStandardError(const QString &)));
}

/*!
 * \brief Destructor
 * \details
 * This function will ask the MPlayer process to quit and block until it has really
 * finished.
 */
void QMPnowidget::kill(){
    m_process->kill();
}

QMPnowidget::~QMPnowidget()
{
    if (m_process->processState() == QProcess::Running) {
        m_process->quit();
    }
    delete m_process;
}

/*!
 * \brief Returns the current MPlayer process state
 *
 * \returns The process state
 */
QMPProcessNowidget::State QMPnowidget::state() const
{
    return m_process->m_state;
}

/*!
 * \brief Returns the current media info object
 * \details
 * Please check QMPnowidget::MediaInfo::ok to make sure the media
 * information has been fully parsed.
 *
 * \returns The media info object
 */
QMPnowidget::MediaInfo QMPnowidget::mediaInfo() const
{
    return m_process->m_mediaInfo;
}

/*!
 * \brief Returns the current playback position
 *
 * \returns The current playback position in seconds
 * \sa seek()
 */
double QMPnowidget::tell() const
{
    return m_process->m_streamPosition;
}

/*!
 * \brief Returns the MPlayer process
 *
 * \returns The MPlayer process
 */
QProcess *QMPnowidget::process() const
{
    return m_process;
}

/*!
 * \brief Sets the path to the MPlayer executable
 * \details
 * Per default, it is assumed the MPlayer executable is
 * available in the current OS path. Therefore, this value is
 * set to "mplayer".
 *
 * \param path Path to the MPlayer executable
 * \sa mplayerPath()
 */
void QMPnowidget::setMPlayerPath(const QString &path)
{
    m_process->m_mplayerPath = path;
}

/*!
 * \brief Returns the current path to the MPlayer executable
 *
 * \returns The path to the MPlayer executable
 * \sa setMPlayerPath()
 */
QString QMPnowidget::mplayerPath() const
{
    return m_process->m_mplayerPath;
}

/*!
 * \brief Returns the version string of the MPlayer executable
 * \details
 * If the mplayer
 *
 *
 * \returns The version string of the MPlayer executable
 */
QString QMPnowidget::mplayerVersion()
{
    return m_process->mplayerVersion();
}

/*!
 * \brief Starts the MPlayer process with the given arguments
 * \details
 * If there's another process running, it will be terminated first. MPlayer
 * will be run in idle mode and is avaiting your commands, e.g. via load().
 *
 * \param args MPlayer command line arguments
 */
void QMPnowidget::start(const QStringList &args)
{
    if (m_process->processState() == QProcess::Running) {
        m_process->quit();
    }
    m_process->start(args);
}

/*!
 * \brief Loads a file or url and starts playback
 *
 * \param url File patho or url
 */
void QMPnowidget::load(const QString &url)
{
    Q_ASSERT_X(m_process->state() != QProcess::NotRunning, "QMPnowidget::load()", "MPlayer process not started yet");

    // From the MPlayer slave interface documentation:
    // "Try using something like [the following] to switch to the next file.
    // It avoids audio playback starting to play the old file for a short time
    // before switching to the new one.
    writeCommand("pausing_keep_force pt_step 1");
    writeCommand("get_property pause");

    writeCommand(QString("loadfile '%1'").arg(url));
}

/*!
 * \brief Resumes playback
 */
void QMPnowidget::play()
{
    if (m_process->m_state == QMPProcessNowidget::PausedState) {
        m_process->pause();
    }
}

/*!
 * \brief Pauses playback
 */
void QMPnowidget::pause()
{
    if (m_process->m_state == QMPProcessNowidget::PlayingState) {
        m_process->pause();
    }
}

/*!
 * \brief Stops playback
 */
void QMPnowidget::stop()
{
    m_process->stop();
}

/*!
 * \brief Media playback seeking
 *
 * \param offset Seeking offset in seconds
 * \param whence Seeking mode
 * \returns \p true If the seeking mode is valid
 * \sa tell()
 */
bool QMPnowidget::seek(int offset, int whence)
{
    return seek(double(offset), whence);
}

/*!
 * \brief Media playback seeking
 *
 * \param offset Seeking offset in seconds
 * \param whence Seeking mode
 * \returns \p true If the seeking mode is valid
 * \sa tell()
 */
bool QMPnowidget::seek(double offset, int whence)
{
    m_seekTimer.stop(); // Cancel all current seek requests

    switch (whence) {
    case RelativeSeek:
    case PercentageSeek:
    case AbsoluteSeek:
        break;
    default:
        return false;
    }

    // Schedule seek request
    m_seekCommand = QString("seek %1 %2").arg(offset).arg(whence);
    m_seekTimer.start();
    return true;
}

/*!
 * \brief Sends a command to the MPlayer process
 * \details
 * Since MPlayer is being run in slave mode, it reads commands from the standard
 * input. It is assumed that the interface provided by this class might not be
 * sufficient for some situations, so you can use this functions to directly
 * control the MPlayer process.
 *
 * For a complete list of commands for MPlayer's slave mode, see
 * http://www.mplayerhq.hu/DOCS/tech/slave.txt .
 *
 * \param command The command line. A newline character will be added internally.
 */
void QMPnowidget::writeCommand(const QString &command)
{
    m_process->writeCommand(command);
}

void QMPnowidget::delayedSeek()
{
    if (!m_seekCommand.isEmpty()) {
        writeCommand(m_seekCommand);
        m_seekCommand = QString();
    }
}

void QMPnowidget::setVolume(int volume)
{
    writeCommand(QString("volume %1 1").arg(volume));
}


//#include "QMPnowidget.moc"


/* Documentation follows */

/*!
 * \class QMPnowidget
 * \brief A Qt widget for embedding MPlayer
 * \details
 *
 * \section Overview
 *
 * \subsection comm MPlayer communication
 *
 * If you want to communicate with MPlayer through its
 * <a href="http://www.mplayerhq.hu/DOCS/tech/slave.txt">slave mode protocol</a>,
 * you can use the writeCommand() slot. If MPlayer writes to its standard output
 * or standard error channel, the signals readStandardOutput() and
 * readStandardError() will be emitted.
 *
 * \subsection controls Graphical controls
 *
 * You can connect sliders for seeking and volume adjustment to an instance of
 * this class. Please use setSeekSlider() and setVolumeSlider(), respectively.
 *
 * \section example Usage example
 *
 * A minimal example using this widget to play a low-res version of
 * <a href="http://www.bigbuckbunny.org/">Big Buck Bunny</a> might look as follows.
 * Please note that the actual movie URL has been shortened for the sake of clarity.
\code
#include <QApplication>
#include "QMPnowidget.h"

// Program entry point
int main(int argc, char **argv)
{
 QApplication app(argc, argv);

 QMPnowidget widget;
 widget.show();
 widget.start(QStringList("http://tinyurl.com/2vs2kg5"));

 return app.exec();
}
\endcode
 *
 *
 * For further information about this project, please refer to the
 * <a href="index.html">main page</a>.
 */

/*!
 * \enum QMPProcessNowidget::State
 * \brief MPlayer state
 * \details
 * This enumeration is somewhat identical to <a href="http://doc.trolltech.com/phonon.html#State-enum">
 * Phonon's State enumeration</a>, except that it has an additional
 * member which is used when the MPlayer process has not been started yet (NotStartedState)
 *
 * <table>
 *  <tr><th>Constant</th><th>Value</th><th>Description</th></tr>
 *  <tr>
 *   <td>\p QMPProcessNowidget::NotStartedState</td>
 *   <td>\p -1</td>
 *   <td>The Mplayer process has not been started yet or has already terminated.</td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPProcessNowidget::IdleState</td>
 *   <td>\p 0</td>
 *   <td>The MPlayer process has been started, but is idle and waiting for commands.</td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPProcessNowidget::LoadingState</td>
 *   <td>\p 1</td>
 *   <td>The media file is being loaded, but playback has not been started yet.</td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPProcessNowidget::StoppedState</td>
 *   <td>\p 2</td>
 *   <td>This constant is deprecated and is not being used</td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPProcessNowidget::PlayingState</td>
 *   <td>\p 3</td>
 *   <td></td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPProcessNowidget::BufferingState</td>
 *   <td>\p 4</td>
 *   <td></td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPProcessNowidget::PausedState</td>
 *   <td>\p 5</td>
 *   <td></td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPProcessNowidget::ErrorState</td>
 *   <td>\p 6</td>
 *   <td></td>
 *  </tr>
 * </table>
 */

/*!
 * \enum QMPnowidget::Mode
 * \brief Video playback modes
 * \details
 * This enumeration describes valid modes for video playback. Please see \ref playbackmodes for a
 * detailed description of both modes.
 *
 * <table>
 *  <tr><th>Constant</th><th>Value</th><th>Description</th></tr>
 *  <tr>
 *   <td>\p QMPnowidget::EmbeddedMode</td>
 *   <td>\p 0</td>
 *   <td>MPlayer will render directly into a Qt widget.</td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPnowidget::PipedMode</td>
 *   <td>\p 1</td>
 *   <td>MPlayer will write the video data into a FIFO which will be parsed in a seperate thread.\n
  The frames will be rendered by QMPnowidget.</td>
 *  </tr>
 * </table>
 */

/*!
 * \enum QMPnowidget::SeekMode
 * \brief Seeking modes
 * \details
 * This enumeration describes valid modes for seeking the media stream.
 *
 * <table>
 *  <tr><th>Constant</th><th>Value</th><th>Description</th></tr>
 *  <tr>
 *   <td>\p QMPnowidget::RelativeSeek</td>
 *   <td>\p 0</td>
 *   <td>Relative seek in seconds</td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPnowidget::PercantageSeek</td>
 *   <td>\p 1</td>
 *   <td>Seek to a position given by a percentage of the whole movie duration</td>
 *  </tr>
 *  <tr>
 *   <td>\p QMPnowidget::AbsoluteSeek</td>
 *   <td>\p 2</td>
 *   <td>Seek to a position given by an absolute time</td>
 *  </tr>
 * </table>
 */

/*!
 * \fn void QMPnowidget::stateChanged(int state)
 * \brief Emitted if the state has changed
 * \details
 * This signal is emitted when the state of the MPlayer process changes.
 *
 * \param state The new state
 */

/*!
 * \fn void QMPnowidget::error(const QString &reason)
 * \brief Emitted if the state has changed to QMPProcessNowidget::ErrorState
 * \details
 * This signal is emitted when the state of the MPlayer process changes to QMPProcessNowidget::ErrorState.
 *
 * \param reason Textual error description (may be empty)
 */

/*!
 * \fn void QMPnowidget::readStandardOutput(const QString &line)
 * \brief Signal for reading MPlayer's standard output
 * \details
 * This signal is emitted when MPlayer wrote a line of text to its standard output channel.
 */

/*!
 * \fn void QMPnowidget::readStandardError(const QString &line)
 * \brief Signal for reading MPlayer's standard error
 * \details
 * This signal is emitted when MPlayer wrote a line of text to its standard error channel.
 */
