#ifndef PORTRANGEGET_H
#define PORTRANGEGET_H

enum PortRangeGet{
    PortRangeGetUnstated=0,
    PortRangeFromServer,
    PortRangeFromVideo,
    PortRangeFromConfig
};

#endif // PORTRANGEGET_H
