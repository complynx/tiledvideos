#ifndef QMPNOWIDGET_H
#define QMPNOWIDGET_H

#include <QObject>
#include <QHash>
#include <QPointer>
#include <QTimer>
#include "qmpprocessnowidget.h"

class QProcess;
class QStringList;


class QMPnowidget : public QObject
{
    Q_OBJECT
    Q_PROPERTY(State state READ state)
    Q_PROPERTY(double streamPosition READ tell)
    Q_PROPERTY(bool multicast READ multicast)
    Q_PROPERTY(QString mplayerPath READ mplayerPath WRITE setMPlayerPath)
    Q_PROPERTY(QString mplayerVersion READ mplayerVersion)

public:
    typedef QMPProcessNowidget::MediaInfo MediaInfo;
    typedef QMPProcessNowidget::State State;

    enum SeekMode {
        RelativeSeek = 0,
        PercentageSeek,
        AbsoluteSeek
    };

public:
    QMPnowidget(QObject *parent = 0);
    virtual ~QMPnowidget();

    QMPProcessNowidget::State state() const;
    MediaInfo mediaInfo() const;
    double tell() const;
    QProcess *process() const;
    bool multicast();

    void setMPlayerPath(const QString &path);
    QString mplayerPath() const;
    QString mplayerVersion();

public slots:
    void start(const QStringList &args = QStringList());
    void load(const QString &url);
    void play();
    void pause();
    void togglePause();
    void stop();
    void kill();
    bool seek(int offset, int whence = AbsoluteSeek);
    bool seek(double offset, int whence = AbsoluteSeek);

    void writeCommand(const QString &command);

private slots:
    void setVolume(int volume);

    void delayedSeek();

signals:
    void stateChanged(int state);
    void error(const QString &reason);

    void readStandardOutput(const QString &line);
    void readStandardError(const QString &line);

private:
    QMPProcessNowidget *m_process;

    QTimer m_seekTimer;
    QString m_seekCommand;
};

#endif // QMPNOWIDGET_H
