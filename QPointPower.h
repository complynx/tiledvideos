#ifndef QPOINTPOWER_H
#define QPOINTPOWER_H

#include <QPoint>
#include <QSize>

inline const QPoint operator*(const QPoint& a,const QPoint& b){
    return QPoint(a.x()*b.x(),a.y()*b.y());
}
inline const QPoint operator*(const QPoint& a,const QSize& b){
    return QPoint(a.x()*b.width(),a.y()*b.height());
}



#endif // QPOINTPOWER_H
