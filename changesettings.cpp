#include "changesettings.h"
#include "ui_changesettings.h"
#include "videomapstate.h"
#include "tiledvideosslavewindow.h"

ChangeSettings::ChangeSettings() :
    QDialog(0),
    ui(new Ui::ChangeSettings)
{
    ui->setupUi(this);
    connect(ui->buttonBox,SIGNAL(clicked(QAbstractButton*)),this,SLOT(save(QAbstractButton*)));
    connect(ui->serverSettings,SIGNAL(toggled(bool)),
            ui->servGroup,SLOT(setEnabled(bool)));
    //connect(parent,SIGNAL(openConfig()),this,SLOT(open()));
}

void ChangeSettings::open(){
    updateFields();
    QDialog::open();
}

void ChangeSettings::updateFields(){
    QSettings s;
    VideomapState *vi=VideomapState::instance();
    ui->Mplayer->setText(s.value(tr("mplayer"),tr("mplayer")).toString());
    ui->videomaps->setText(vi->tilePathTpl());
    QPoint pos=vi->client->instancePosition();
    ui->left->setValue(pos.x());
    ui->top->setValue(pos.y());
    ui->serverSettings->setChecked(vi->state().testFlag(VideomapState::State_Server));
    ui->servGroup->setEnabled(vi->state().testFlag(VideomapState::State_Server));
    ui->serverPort->setValue(vi->serverPort());
    ui->playerPort->setValue(vi->playerPort());
    ui->srange->setValue(vi->serverPortRange());
    ui->prange->setValue(vi->playerPortRange());
    ui->pingLimit->setValue(vi->pingLimit());
    ui->pingInterval->setValue(vi->pingInterval());
    ui->stateEchoInterval->setValue(vi->stateEchoInterval());
    //PortRangeGet state=(PortRangeGet)vi->settings.value("portRangeGet",PortRangeFromServer).toInt();
    //rangeGetChanged((int)state-1);
    //ui->fixed->setChecked(parent->settings.value("fixedTileSize",false).toBool());
    //tsizeGetChanged(ui->fixed->isChecked());
//    ui->height->setEnabled(ui->fixed->isChecked());
//    ui->width->setEnabled(ui->fixed->isChecked());
//    ui->server->setValue(vi->settings.value("serverport",23865).toInt());

}

void ChangeSettings::save(){
    QSettings s;
    VideomapState *vi=VideomapState::instance();
    s.setValue("mplayer",ui->Mplayer->text());
    vi->setTilePath(ui->videomaps->text());
    vi->client->setInstancePosition(QPoint(
        ui->left->value(),
        ui->top->value()
    ));
    if(ui->serverSettings->isChecked()){
        vi->setServerPort(ui->serverPort->value());
        vi->setPlayerPort(ui->playerPort->value());
        vi->setServerPortRange(ui->srange->value());
        vi->setPlayerPortRange(ui->prange->value());
        vi->setPingLimit(ui->pingLimit->value());
        vi->setPingInterval(ui->pingInterval->value());
        vi->setStateEchoInterval(ui->stateEchoInterval->value());
    }
    emit saved();
}

void ChangeSettings::save(QAbstractButton*btn){
    QDialogButtonBox::StandardButton role=ui->buttonBox->standardButton(btn);
    if(role==QDialogButtonBox::Apply || role==QDialogButtonBox::Ok){
        save();
    }
}

ChangeSettings::~ChangeSettings()
{
    delete ui;
}
