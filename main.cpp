#include <QtGui/QApplication>
#include "videomapstate.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setOrganizationName("Complynx");
    QApplication::setOrganizationDomain("complynx.net");
    QApplication::setApplicationName("Tiled Videos");

    VideomapState::instance();
    
    return a.exec();
}
